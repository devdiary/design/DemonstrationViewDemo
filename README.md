### [DeveloperDiary](https://gitlab.com/devdiary)
## [Design](https://gitlab.com/devdiary/design)
# DemonstrationViewDemo
Demo app for example using special DemonstrationView - realization [design concept](https://www.uplabs.com/posts/slider-concept-for-the-foreign-language-school)

<div align="center">
  <img src="media/icon.png"/>
</div>
<div align="center">
  <strong>source for <a href="https://medium.com/developerdiary/design">medium</a></strong>
</div>
