package devdiary.design.dmvw;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import devdiary.design.dmvw.views.DemonstrationView;

public class MainActivity
    extends Activity
{
    private final View.OnClickListener onClickListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
            switch(view.getId())
            {
                case R.id.open_close:
                    if(demonstration.isOpened())
                    {
                        demonstration.close();
                    }
                    else
                    {
                        demonstration.open();
                    }
                    break;
            }
        }
    };

    private DemonstrationView demonstration;

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.main_screen);
        findViewById(R.id.open_close).setOnClickListener(onClickListener);
        demonstration = findViewById(R.id.demonstration);
    }
}