package devdiary.design.dmvw.views;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class DemonstrationView
    extends View
{
    private boolean opened;
    private int linesCount;
    private final Paint linesPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private float currentProgress;
    private long animationTime;
    private final ObjectAnimator currentAnimator = ObjectAnimator.ofFloat(this, "currentProgress", 0);

    public DemonstrationView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }
    private void init()
    {
        opened = true;
        linesCount = 5;
        linesPaint.setColor(Color.parseColor("#ffffff"));
        currentProgress = 0;
        animationTime = 500;
    }

    protected void onDraw(Canvas canvas)
    {
        float lineSize = getHeight();
        lineSize /= linesCount;
        for(int i=0; i<linesCount; i++)
        {
            canvas.drawRect(0, i*lineSize, (float)(Math.pow(currentProgress, i+1)*getWidth()), i*lineSize + lineSize, linesPaint);
        }
    }

    public void open()
    {
        if(opened)
        {
            return;
        }
        opened = true;
        startNewAnimation(0, (long)(currentProgress*animationTime));
    }
    public void close()
    {
        if(!opened)
        {
            return;
        }
        opened = false;
        startNewAnimation(1, (long)((1-currentProgress)*animationTime));
    }
    public boolean isOpened()
    {
        return opened;
    }

    private void startNewAnimation(float to, long duration)
    {
        currentAnimator.cancel();
        currentAnimator.setFloatValues(currentProgress, to);
        currentAnimator.setDuration(duration);
        currentAnimator.start();
    }
    private void setCurrentProgress(float progress)
    {
        currentProgress = progress;
        invalidate();
    }
}